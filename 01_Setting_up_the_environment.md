# Intro to ChefDK

### What is ChefDK

It is the Chef Development Kit, it includes:

+ A built-in Ruby runtime
+ chef-client and Ohai
+ Testing tools:
 + Test Kitchen
 + ChefSpec
 + Rubocop
 + Foodcritic

Everything else needed to author cookbooks and upload them to the Chef server

A computer with the ChefDK is called a Chef Workstation

The more important tools included are:
+ **Berkshelf**: a dependency manager for cookbooks
+ **chef**: a workflow tool for chef
+ **chef-client**: the agent that runs chef
+ **chef-vault**: tool to encrypt data bag items
+ **ChefSpec**: a unit testing framework that tests resources locally
+ **Fauxhai**: a gem for mocking Ohai data in ChefSpec tests
+ **Foodcritic**: a lint tool for static analysis of recipe code
+ **Test Kitchen**: an integration testing framework tool that tests cookbooks across platforms
+ **knife-spork**: a workflow plugin for knife that helps groups of people work together in the same repo
+ **Ruby**: the reference language for chef
+ **Rubocop**: Ruby style checking tool

Community tools included with ChefDK:

Berkshelf
chef-vault
ChefSpec
Foodcritic
Test Kitchen
Rubocop

# Workstation configuration

### Install ChefDK

```
curl -s https://omnitruck.chef.io/install.sh | sudo bash -s -- -P chefdk
echo 'eval "$(chef shell-init bash)"' >> ~/.bash_profile
source ~/.bash_profile
```

### Install Docker

```
sudo yum install -y git yum-utils
sudo yum-config-manager --add-repo \
https://download.docker.com/linux/centos/docker-ce.repo

sudo yum makecache fast
sudo yum -y install docker-ce
sudo systemctl enable docker
sudo systemctl start docker

sudo usermod -aG docker $USER

sudo systemctl stop getty@tty1.service
sudo systemctl mask getty@tty1.service

logout
```

Login back, then run 

```
docker network create --subnet=10.1.1.0/24 testnet
gem install kitchen-docker
```

### Configure Git

```
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
git config --global core.editor vim
git config --global color.ui auto
```

